#!/bin/bash

output_dir=$1
file=$2

# Path_vcftools="/isilon/sequencing/Kurt/Programs/PATH/vcftools-0.1.12b"

module load vcftools/0.1.12b

vcftools --vcf $2 --freq --out ../$output_dir/vcftools_SNV/freq
vcftools --vcf $2 --freq2 --out ../$output_dir/vcftools_SNV/freq2
vcftools --vcf $2 --counts --out ../$output_dir/vcftools_SNV/counts
vcftools --vcf $2 --counts2 --out ../$output_dir/vcftools_SNV/counts2
vcftools --vcf $2 --depth --out ../$output_dir/vcftools_SNV/depth
vcftools --vcf $2 --site-depth --out ../$output_dir/vcftools_SNV/site_depth
vcftools --vcf $2 --site-mean-depth --out ../$output_dir/vcftools_SNV/site_mean_depth
vcftools --vcf $2 --site-quality --out ../$output_dir/vcftools_SNV/site_quality
vcftools --vcf $2 --geno-depth --out ../$output_dir/vcftools_SNV/geno_depth
vcftools --vcf $2 --het --out ../$output_dir/vcftools_SNV/het
vcftools --vcf $2 --hardy --out ../$output_dir/vcftools_SNV/hardy
vcftools --vcf $2 --missing-indv --out ../$output_dir/vcftools_SNV/missing
vcftools --vcf $2 --missing-site --out ../$output_dir/vcftools_SNV/missing
#vcftools --vcf $2 --geno-r2 --out ../$output_dir/vcftools_SNV/geno_r2
vcftools --vcf $2 --SNPdensity 1000000 --out ../$output_dir/vcftools_SNV/SNPdensity_1000000
vcftools --vcf $2 --TsTv 1000000 --out ../$output_dir/vcftools_SNV/TsTv_1000000
vcftools --vcf $2 --site-pi --out ../$output_dir/vcftools_SNV/site_pi
vcftools --vcf $2 --singletons --out ../$output_dir/vcftools_SNV/singletons
#vcftools --vcf $2 --chr 1 --BEAGLE-GL --out ../$output_dir/vcftools_SNV/BEAGLE_GL
#vcftools --vcf $2 --plink --out ../$output_dir/vcftools_SNV/plink
#vcftools --vcf $2 --plink-tped --out ../$output_dir/vcftools_SNV/plink_tped
#vcftools --vcf $2 --relatedness --out ../$output_dir/vcftools_SNV/relatedness



#vcftools --vcf $2 --LROH --chr 1 --out ../$output_dir/vcftools_SNV/LROH_chr1
#vcftools --vcf $2 --LROH --chr 2 --out ../$output_dir/vcftools_SNV/LROH_chr2
#vcftools --vcf $2 --LROH --chr 3 --out ../$output_dir/vcftools_SNV/LROH_chr3
#vcftools --vcf $2 --LROH --chr 4 --out ../$output_dir/vcftools_SNV/LROH_chr4
#vcftools --vcf $2 --LROH --chr 5 --out ../$output_dir/vcftools_SNV/LROH_chr5

#vcftools --vcf $2 --LROH --chr 6 --out ../$output_dir/vcftools_SNV/LROH_chr6
#vcftools --vcf $2 --LROH --chr 7 --out ../$output_dir/vcftools_SNV/LROH_chr7
#vcftools --vcf $2 --LROH --chr 8 --out ../$output_dir/vcftools_SNV/LROH_chr8
#vcftools --vcf $2 --LROH --chr 9 --out ../$output_dir/vcftools_SNV/LROH_chr9
#vcftools --vcf $2 --LROH --chr 10 --out ../$output_dir/vcftools_SNV/LROH_chr10

#vcftools --vcf $2 --LROH --chr 11 --out ../$output_dir/vcftools_SNV/LROH_chr11
#vcftools --vcf $2 --LROH --chr 12 --out ../$output_dir/vcftools_SNV/LROH_chr12
#vcftools --vcf $2 --LROH --chr 13 --out ../$output_dir/vcftools_SNV/LROH_chr13
#vcftools --vcf $2 --LROH --chr 14 --out ../$output_dir/vcftools_SNV/LROH_chr14
#vcftools --vcf $2 --LROH --chr 15 --out ../$output_dir/vcftools_SNV/LROH_chr15

#vcftools --vcf $2 --LROH --chr 16 --out ../$output_dir/vcftools_SNV/LROH_chr16
#vcftools --vcf $2 --LROH --chr 17 --out ../$output_dir/vcftools_SNV/LROH_chr17
#vcftools --vcf $2 --LROH --chr 18 --out ../$output_dir/vcftools_SNV/LROH_chr18
#vcftools --vcf $2 --LROH --chr 19 --out ../$output_dir/vcftools_SNV/LROH_chr19
#vcftools --vcf $2 --LROH --chr 20 --out ../$output_dir/vcftools_SNV/LROH_chr20

#vcftools --vcf $2 --LROH --chr 21 --out ../$output_dir/vcftools_SNV/LROH_chr21
#vcftools --vcf $2 --LROH --chr 22 --out ../$output_dir/vcftools_SNV/LROH_chr22
#vcftools --vcf $2 --LROH --chr X --out ../$output_dir/vcftools_SNV/LROH_chrX
