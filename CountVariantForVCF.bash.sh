#!/bin/csh

##{1}: refers to the Path to BEDsuperset.VQSR.vcf file
##{2}: refers to the name of BEDsuperset.VQSR.vcf file
##{3}: refers to the name of SNV.VQSR.vcf file
##{4}: refers to the name of INDEL.VQSR.vcf file

##{5}: # for Callset
##{6}: # for Mendel Samples
##{7}: # for HapMap Samples


rm -f CountVarByBatch_${5}.txt

## All Sample VCF files

grep -v "^#" $1/$2 | wc -l | awk '{print "Count of All Variants" "\t" $1}' > CountVarByBatch_$5.txt
grep -v "^#" $1/$2 | awk '$7=="PASS" {print $1}' | wc -l | awk '{print "Count of All Variants PASS" "\t" $1}' >> CountVarByBatch_$5.txt
grep -v "^#" $1/$2 | awk '$7!="PASS" {print $1}' | wc -l | awk '{print "Count of All Variants FAIL" "\t" $1}' >> CountVarByBatch_$5.txt


grep -v "^#" $1/$3 | wc -l | awk '{print "Count of All SNVs" "\t" $1}' >> CountVarByBatch_${5}.txt
grep -v "^#" $1/$3 | awk '$7=="PASS" {print $1}' | wc -l | awk '{print "Count of All SNVs PASS" "\t" $1}' >> CountVarByBatch_$5.txt
grep -v "^#" $1/$3 | awk '$7!="PASS" {print $1}' | wc -l | awk '{print "Count of All SNVs FAIL" "\t" $1}' >> CountVarByBatch_$5.txt


grep -v "^#" $1/$4 | wc -l | awk '{print "Count of All INDELs" "\t" $1}' >> CountVarByBatch_$5.txt
grep -v "^#" $1/$4 | awk '$7=="PASS" {print $1}' | wc -l | awk '{print "Count of All INDELs PASS" "\t" $1}' >> CountVarByBatch_$5.txt
grep -v "^#" $1/$4 | awk '$7!="PASS" {print $1}' | wc -l | awk '{print "Count of All INDELs FAIL" "\t" $1}' >> CountVarByBatch_$5.txt


## Mendel Samples Only
grep -v "^#" /isilon/sequencing/Seq_Proj/M_Valle_MendelianDisorders_SeqWholeExome_120511_GATK_3_3-0/MULTI_SAMPLE/HL/${5}Callset_${6}Mendel/${5}Callset_${6}Mendel.PASS.SNV.vcf | wc -l | awk '{print "Count of All SNVs PASS among Mendel Samples:" "\t" $1}' >> CountVarByBatch_${5}.txt
grep -v "^#" /isilon/sequencing/Seq_Proj/M_Valle_MendelianDisorders_SeqWholeExome_120511_GATK_3_3-0/MULTI_SAMPLE/HL/${5}Callset_${6}Mendel/${5}Callset_${6}Mendel.PASS.INDEL.vcf | wc -l | awk '{print "Count of All INDELs PASS among Mendel Samples:" "\t" $1}' >> CountVarByBatch_${5}.txt

## HapMap samples Only
grep -v "^#" /isilon/sequencing/Seq_Proj/M_Valle_MendelianDisorders_SeqWholeExome_120511_GATK_3_1-1/MULTI_SAMPLE/HL/${5}Callset_${7}HapMap/${5}Callset_${7}HapMap.PASS.SNV.vcf | wc -l | awk '{print "Count of All SNVs PASS among HapMap Samples" "\t" $1}' >> CountVarByBatch_${5}.txt
grep -v "^#" /isilon/sequencing/Seq_Proj/M_Valle_MendelianDisorders_SeqWholeExome_120511_GATK_3_1-1/MULTI_SAMPLE/HL/${5}Callset_${7}HapMap/${5}Callset_${7}HapMap.PASS.INDEL.vcf | wc -l | awk '{print "Count of All INDELs PASS among HapMap Samples" "\t" $1}' >> CountVarByBatch_${5}.txt

