#!/bin/bash

VCF_SNV=$1
VCF_INDEL=$2
NumSample_Total=$3
NumSample_Mendel=$4
NumSample_HapMap=$5
Type=$6 ## options are "all, mendel, hapmap"

Path_vcftools="/isilon/sequencing/Kurt/Programs/PATH/vcftools-0.1.12b"
# using a module instead




if [ ! -d ../"${NumSample_Total}Callset" ]; then
	mkdir ../"${NumSample_Total}Callset"
fi

if [ ! -d ../"${NumSample_Total}Callset_${NumSample_Mendel}Mendel" ]; then
	mkdir ../"${NumSample_Total}Callset_${NumSample_Mendel}Mendel"
fi

if [ ! -d ../"${NumSample_Total}Callset_${NumSample_HapMap}HapMap" ]; then
	mkdir ../"${NumSample_Total}Callset_${NumSample_HapMap}HapMap"
fi

if [ $Type == "all" ]; then
	output_dir="${NumSample_Total}Callset"
elif [ $Type == "mendel" ]; then
	output_dir="${NumSample_Total}Callset_${NumSample_Mendel}Mendel"
else 
	output_dir="${NumSample_Total}Callset_${NumSample_HapMap}HapMap"
fi

cp -r ../Template/* ../$output_dir

./ApplyVCFtools_SNV.sh $output_dir $VCF_SNV
./ApplyVCFtools_INDEL.sh $output_dir $VCF_INDEL
