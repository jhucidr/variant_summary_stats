#!/bin/bash
Num_Callset=$1
Num_Mendel=$2
Num_HapMap=$3

mkdir ../${1}Callset
mkdir ../${1}Callset_${2}Mendel
mkdir ../${1}Callset_${3}HapMap

cp -pr ../Template/* ${1}Callset
cp -pr ../Template/* ${1}Callset_${2}Mendel
cp -pr ../Template/* ${1}Callset_${3}HapMap

