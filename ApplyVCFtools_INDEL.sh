#!/bin/bash

output_dir=$1

module load vcftools/0.1.12b

# Path_vcftools="/isilon/sequencing/Kurt/Programs/PATH/vcftools-0.1.12b"

vcftools --vcf $2 --freq --out ../$output_dir/vcftools_INDEL/freq
vcftools --vcf $2 --freq2 --out ../$output_dir/vcftools_INDEL/freq2
vcftools --vcf $2 --counts --out ../$output_dir/vcftools_INDEL/counts
vcftools --vcf $2 --counts2 --out ../$output_dir/vcftools_INDEL/counts2
vcftools --vcf $2 --depth --out ../$output_dir/vcftools_INDEL/depth
vcftools --vcf $2 --site-depth --out ../$output_dir/vcftools_INDEL/site_depth
vcftools --vcf $2 --site-mean-depth --out ../$output_dir/vcftools_INDEL/site_mean_depth
vcftools --vcf $2 --site-quality --out ../$output_dir/vcftools_INDEL/site_quality
vcftools --vcf $2 --geno-depth --out ../$output_dir/vcftools_INDEL/geno_depth
vcftools --vcf $2 --het --out ../$output_dir/vcftools_INDEL/het
vcftools --vcf $2 --hardy --out ../$output_dir/vcftools_INDEL/hardy
vcftools --vcf $2 --missing-indv --out ../$output_dir/vcftools_INDEL/missing
vcftools --vcf $2 --missing-site --out ../$output_dir/vcftools_INDEL/missing
#vcftools --vcf $2 --geno-r2 --out ../$output_dir/vcftools_INDEL/geno_r2
vcftools --vcf $2 --SNPdensity 1000000 --out ../$output_dir/vcftools_INDEL/SNPdensity_1000000
vcftools --vcf $2 --TsTv 1000000 --out ../$output_dir/vcftools_INDEL/TsTv_1000000
vcftools --vcf $2 --site-pi --out ../$output_dir/vcftools_INDEL/site_pi
vcftools --vcf $2 --singletons --out ../$output_dir/vcftools_INDEL/singletons
vcftools --vcf $2 --chr 1 --BEAGLE-GL --out ../$output_dir/vcftools_INDEL/BEAGLE_GL
vcftools --vcf $2 --plink --out ../$output_dir/vcftools_INDEL/plink
vcftools --vcf $2 --plink-tped --out ../$output_dir/vcftools_INDEL/plink_tped
vcftools --vcf $2 --relatedness --out ../$output_dir/vcftools_INDEL/relatedness



